package domain;

public class RolePermission {
    private int id;
    private int idx_role;
    private int idx_permission;

    public RolePermission(int id, int idx_role, int idx_permission) {
        this.id = id;
        this.idx_role = idx_role;
        this.idx_permission = idx_permission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdx_role() {
        return idx_role;
    }

    public void setIdx_role(int idx_role) {
        this.idx_role = idx_role;
    }

    public int getIdx_permission() {
        return idx_permission;
    }

    public void setIdz_permission(int idx_permission) {
        this.idx_permission = idx_permission;
    }
}
