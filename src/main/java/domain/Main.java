package domain;

public class Main {

    public static void main(String[] args) {

        Address adr = new Address("Berlin","Kalemba 12","44-580");
        Permission perm = new Permission("odczyt");
        Permission perm2 = new Permission("execute");
        Role role = new Role();
        role.addPermissions(perm);
        role.addPermissions(perm2);
        Person person = new Person("Gienek","Loak","589658785");
        person.addAddress(adr);
        person.addRole(role);
        User usr = new User("hah","hehe");
        usr.setPerson(person);
        System.out.println(usr.toString());
    }
}
