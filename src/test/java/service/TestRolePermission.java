package service;

import domain.Permission;
import domain.Role;
import domain.RolePermission;
import domain.User;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestRolePermission {


    RolePermissionManager rolePermissionManager = new RolePermissionManager();

    private final static int IDX_ROLE_1 = 1;
    private final static int ID_ROLE_1 = 1;
    private final static int ID_1 = 5;
    private final static int IDX_PERMISSION_1 = 2;
    private final static int ID_PERMISSION_1 = 2;
    private final static String ROLE_1 = "user";
    private final static String PERMISSION_1 = "execute";

    @Test
    public void checkConnection(){
        assertNotNull(rolePermissionManager.getConnection());
    }

    @Test
    public void checkAdding(){

        Role role = new Role(ROLE_1);
        role.setId(ID_ROLE_1);
        Permission permission = new Permission(PERMISSION_1);
        permission.setId(ID_PERMISSION_1);
        role.addPermissions(permission);

        RolePermission rolePermission = new RolePermission(ID_1,IDX_ROLE_1, IDX_PERMISSION_1);

        rolePermissionManager.clearRolesPerms();
        assertEquals(1,rolePermissionManager.addRolePerm(role,permission));

        List<RolePermission> rolePermissions = rolePermissionManager.getAllRolePermission();
        RolePermission rolePermissionRetrieved = rolePermissions.get(0);

        assertEquals(IDX_PERMISSION_1, rolePermissionRetrieved.getIdx_permission());
        assertEquals(IDX_ROLE_1, rolePermissionRetrieved.getIdx_role());

    }

}
